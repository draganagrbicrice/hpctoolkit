// SPDX-FileCopyrightText: 2002-2024 Rice University
// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*- // technically C99

#ifndef gpu_metric_names_h
#define gpu_metric_names_h



//*****************************************************************************
// macros
//*****************************************************************************

#define GPU_INST_METRIC_NAME "GINS"

#endif
