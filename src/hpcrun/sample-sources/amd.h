// SPDX-FileCopyrightText: 2019-2024 Rice University
// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

//
// Created by user on 17.8.2019..
//

#ifndef HPCTOOLKIT_AMD_H
#define HPCTOOLKIT_AMD_H


typedef struct gpu_activity_t gpu_activity_t;
typedef struct cct_node_t cct_node_t;

void roctracer_init();
void roctracer_fini();


#endif //HPCTOOLKIT_AMD_H
