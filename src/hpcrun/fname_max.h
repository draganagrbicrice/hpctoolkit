// SPDX-FileCopyrightText: 2002-2024 Rice University
// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*- // technically C99

#ifndef FNAME_MAX_H

#ifndef NAME_MAX
#define NAME_MAX 1024
#endif  // NAME_MAX

#define HPCRUN_FNM_SZ  (NAME_MAX+1) /* filename size */

#endif // FNAME_MAX_H
