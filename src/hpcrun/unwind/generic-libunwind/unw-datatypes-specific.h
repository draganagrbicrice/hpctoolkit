// SPDX-FileCopyrightText: 2012-2024 Rice University
// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

//
// This software was produced with support in part from the Defense Advanced
// Research Projects Agency (DARPA) through AFRL Contract FA8650-09-C-1915.
// Nothing in this work should be construed as reflecting the official policy or
// position of the Defense Department, the United States government, or
// Rice University.
//
#ifndef UNW_DATATYPES_SPECIFIC_H
#define UNW_DATATYPES_SPECIFIC_H

#include "../common/std_unw_cursor.h"

#endif // UNW_DATATYPES_SPECIFIC_H
