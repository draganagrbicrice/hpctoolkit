// SPDX-FileCopyrightText: 2002-2024 Rice University
// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*- // technically C99

#ifndef gpu_operation_item_process_h
#define gpu_operation_item_process_h



//******************************************************************************
// forward type declarations
//******************************************************************************

typedef struct gpu_operation_item_t gpu_operation_item_t;



//******************************************************************************
// interface operations
//******************************************************************************

void
gpu_operation_item_process
(
 gpu_operation_item_t *it
);



#endif
