// SPDX-FileCopyrightText: 2002-2024 Rice University
// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*- // technically C99

#ifndef HPCRUN_MONITOR_EXTS_OPENMP_H
#define HPCRUN_MONITOR_EXTS_OPENMP_H

extern void
foilbase__mp_init(void);

#endif  // HPCRUN_MONITOR_EXTS_OPENMP_H
