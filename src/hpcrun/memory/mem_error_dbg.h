// SPDX-FileCopyrightText: 2002-2024 Rice University
// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*- // technically C99

#ifndef MEM_ERROR_DBG
#define MEM_ERROR_DBG

// always define these values (as #if, not #ifdef is used)
//  should define to 0, unless testing memory allocation failure

#define USE_SMALL_MEM 0
#define GEN_INF_MEM_REQ 0

#endif // MEM_ERROR_DBG
